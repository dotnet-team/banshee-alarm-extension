using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("banshee-alarm-extension")]
[assembly: AssemblyDescription("Alarm & Sleep Timer extension for Banshee")]
[assembly: AssemblyVersion("0.4.4")]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]
